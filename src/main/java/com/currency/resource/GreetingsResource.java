package com.currency.resource;

import com.currency.dto.GreetingDto;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/greeting")
public class GreetingsResource {

    private static final String template = "Hello, %s!";

    @RequestMapping(method=GET)
    public GreetingDto greeting(@RequestParam(value="name", defaultValue="Guest") String name){
        GreetingDto response = new GreetingDto();
        response.setMessage(String.format(template, name));
        return response;
    }
}
